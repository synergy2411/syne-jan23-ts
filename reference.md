# JavaScript-

- let / const for variable declaration (Block-scoped variables)
- var keyword (function scoped)
- function expression and declaration
- Array Methods - map, filter, find, findIndex, reduce, splice, slice, indexOf, concat etc
- Rest / Spread Operator (...)
- Shallow and deep copy
- Arrow Functions
- Closures
- Callback
- Promises
- JSON.stringify() / JSON.parse()
- String Methods - charAt, toUpperCase(), toLowerCase(), indexOf() etc
- Number - toFixed, isNan
- Boolean - isBoolean
- Date - setMonth, setYear, getFullYear etc.
- Function - call, bind, apply
- Timer API - setTimeout, setInterval

# Types in JavaScript

- Primitive Type > String, Number, Boolean, Symbol
- Reference Type > Object, Array, Function, Date

# Browser Environment : Client side script

# Node Runtime Environment : Server side script

MEAN -> MongoDB, Express, Angular, Node

# TypeScript Compiler

> npm i typescript -g
> tsc main.ts

# Commands

> npm init -y -> Generate Package.JSON file
> npm i colors
> import "colors";
> tsc main.ts --outDir build --sourceMap true --declaration true
> tsc main.ts --help

> chalk - TS
> npm i chalk
> colors - DT
> npm i colors @types/colors react @types/react

CommonJS Module

- by default in Node
- require() and module.exports keyword

ESM Module / ES6/ES2015

- by default in browser
- import statement & export statement

> npm i nodemon
> tsc --init
> npm run build:watch
> npm run dev:start

# Class in TypeScript

- Access Modifiers
  > public : can access outside class
  > private : can use it within the class
  > protected : can use in class and its subclasses
  > readonly : make immutable

# TypeScript -> JavaScript extension for types

# Various Types

- Unknown : can't be assigned to other type variables until we check the type of unknown variable
- any : can accept any value and can be assigned to other type variables
- string : to hold string
- number : to hold number value
- boolean : to hold boolean value
- object : to hold the object
- Function : to hold the function value
- Array : to hold the array
- Tuple : to hold array of different types and restricted number of elements
- Enum : to hold constants
- Undefined : special value supplied by the JAvaScript Engine
- Null : represent absence of value
- Never : function that always throw the error
- Void : does not return anything
- Union : (|) OR Condition. either of the type will be acceptable
- type keyword : custom type

# ES6+ features

- const / let (Block scope variables)
- String Template Literal (` `)
- Destructuring
- Rest / Spread (...)
- Classes

  > constructor
  > accessor / mutator (Getters/Setters)
  > inheritance (using extends)
  > access modifiers (public, private, protected, readonly)
  > abstract class
  > static properties and static methods
  > interface

- function overloading
- generics
- decorators
- modules
- setup for browser
- browser app

# Decorators : simple function to add meta-programming

- Class
- Property
- Method
- Parameter of Methods
- Property Accessor
