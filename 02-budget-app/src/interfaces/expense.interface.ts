
export interface IExpense {
    title: string;
    amount: number;
    createdAt: Date;
}