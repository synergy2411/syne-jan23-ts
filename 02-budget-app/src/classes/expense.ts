import { IExpense } from '../interfaces/expense.interface.js';
import { Formatter } from '../interfaces/formatter.interface.js';

class Expense implements Formatter {
    constructor(private expense: IExpense) { }
    format(): string {
        return `You paid INR${this.expense.amount} at ${this.expense.createdAt.toString()} on account of ${this.expense.title.toUpperCase()}`
    };
}

export default Expense;