import { IExpense } from '../interfaces/expense.interface.js';
import { Formatter } from '../interfaces/formatter.interface.js';

class Income implements Formatter {
    constructor(private expense: IExpense) { }
    format(): string {
        return `You received INR${this.expense.amount} on ${this.expense.createdAt.toString()} on account of ${this.expense.title.toUpperCase()}`
    };
}

export default Income;