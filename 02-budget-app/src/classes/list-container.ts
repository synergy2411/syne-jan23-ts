import { Formatter } from "../interfaces/formatter.interface.js";

class ListTemplate {
    constructor(private ulContainer: HTMLUListElement) { }
    render(expenseType: string, doc: Formatter) {
        const liElement = document.createElement("li") as HTMLLIElement;
        liElement.classList.add("list-group-item", "mb-4");
        liElement.innerHTML = `
            <div class='d-flex align-item-center'>
                <h5>${expenseType.toUpperCase()}</h5>
                <p class='p-4'>${doc.format()}</p>
            </div>
        `
        this.ulContainer.appendChild(liElement);
    }
}

export default ListTemplate;