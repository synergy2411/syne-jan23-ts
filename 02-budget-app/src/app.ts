import { IExpense } from './interfaces/expense.interface.js';
import { Formatter } from './interfaces/formatter.interface.js';
import Income from './classes/income.js';
import Expense from './classes/expense.js';
import ListTemplate from './classes/list-container.js';


const btnAdd = document.getElementById("btn-add") as HTMLButtonElement;
const expenseType = document.getElementById("expType") as HTMLSelectElement;
const inputTitle = document.querySelector("#title") as HTMLInputElement;
const inputAmount = document.querySelector("#amount") as HTMLInputElement;
const inputCreatedAt = document.querySelector("#createdAt") as HTMLInputElement;
const listContainer = document.getElementById("list-container") as HTMLUListElement;

let listTemplateObj = new ListTemplate(listContainer)

btnAdd.addEventListener("click", function (ev: MouseEvent) {
    ev.preventDefault();
    let doc: Formatter;
    let expense: IExpense = {
        title: inputTitle.value,
        amount: Number(inputAmount.value),
        createdAt: new Date(inputCreatedAt.value)
    }
    if (expenseType.value === "income") {
        doc = new Income(expense)
    } else {
        doc = new Expense(expense)
    }
    listTemplateObj.render(expenseType.value, doc)
})