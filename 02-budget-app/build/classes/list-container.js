class ListTemplate {
    constructor(ulContainer) {
        this.ulContainer = ulContainer;
    }
    render(expenseType, doc) {
        const liElement = document.createElement("li");
        liElement.classList.add("list-group-item", "mb-4");
        liElement.innerHTML = `
            <div class='d-flex align-item-center'>
                <h5>${expenseType.toUpperCase()}</h5>
                <p class='p-4'>${doc.format()}</p>
            </div>
        `;
        this.ulContainer.appendChild(liElement);
    }
}
export default ListTemplate;
