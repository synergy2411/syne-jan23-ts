class Income {
    constructor(expense) {
        this.expense = expense;
    }
    format() {
        return `You received INR${this.expense.amount} on ${this.expense.createdAt.toString()} on account of ${this.expense.title.toUpperCase()}`;
    }
    ;
}
export default Income;
