class Expense {
    constructor(expense) {
        this.expense = expense;
    }
    format() {
        return `You paid INR${this.expense.amount} at ${this.expense.createdAt.toString()} on account of ${this.expense.title.toUpperCase()}`;
    }
    ;
}
export default Expense;
