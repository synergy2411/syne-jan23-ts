import Income from './classes/income.js';
import Expense from './classes/expense.js';
import ListTemplate from './classes/list-container.js';
const btnAdd = document.getElementById("btn-add");
const expenseType = document.getElementById("expType");
const inputTitle = document.querySelector("#title");
const inputAmount = document.querySelector("#amount");
const inputCreatedAt = document.querySelector("#createdAt");
const listContainer = document.getElementById("list-container");
let listTemplateObj = new ListTemplate(listContainer);
btnAdd.addEventListener("click", function (ev) {
    ev.preventDefault();
    let doc;
    let expense = {
        title: inputTitle.value,
        amount: Number(inputAmount.value),
        createdAt: new Date(inputCreatedAt.value)
    };
    if (expenseType.value === "income") {
        doc = new Income(expense);
    }
    else {
        doc = new Expense(expense);
    }
    listTemplateObj.render(expenseType.value, doc);
});
