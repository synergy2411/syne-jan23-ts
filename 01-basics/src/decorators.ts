
// @ClassDecorator
// class Test{

//     @PropertyDecorator
//     _username : string;

//     @MethodDecorator
//     getUser( args1 : string, @PropertyDecorator args2 : string ){}

//     @AccessorDecorator
//     get username(){
//         return this._username;
//     }

// }

// Decorator Factory
// function Seal(val) {
//     return (target) => {
//         Object.seal(target);
//         Object.seal(target.prototype);
//     }
// }

// @Seal({})
// class Test {}

let myFriends = ["Joe", "Monica", "Ross"];

function FriendOnly(oldFriend) {
    return (target: any, propertyName: string) => {
        let currentValue = target[propertyName] || oldFriend;

        Object.defineProperty(target, propertyName, {
            get: () => currentValue,
            set: (newValue) => {
                if (myFriends.includes(newValue)) {
                    currentValue = newValue
                }
                return;
            }
        })
    }
}


function PrintArgs(target: any, propertyName: string, paramIndex: number) {
    console.log("Index : ", paramIndex)
}

class MyFriends {
    @FriendOnly("Joe")
    friendName: string;

    getFriend(args: string, @PrintArgs args2: string) {
        console.log("ARGS : ", args)
        console.log("ARGS 02: ", args2)
    }
}

let theFriend = new MyFriends();
theFriend.friendName = "Xyz"
console.log("After XYZ : ", theFriend.friendName);
theFriend.friendName = "Monica"
console.log("After Monica : ", theFriend.friendName);
theFriend.getFriend("Rachel", "Chandler")