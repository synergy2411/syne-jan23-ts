// class Person {
//     // private firstName: string;
//     // private lastName: string;
//     // private age: number;

//     // constructor(firstName: string, lastName: string, age: number) {
//     // this.firstName = firstName;
//     // this.lastName = lastName;
//     // this.age = age;
//     // }

//     // readonly isGraduated: boolean;
//     private _age: number;
//     // Constructor Injection
//     constructor(
//         private firstName: string,
//         private lastName: string,
//         readonly isGraduated: boolean) {
//     }

//     get age() {
//         return this._age;
//     }

//     set age(val: number) {
//         if (val < 18 || val > 60) {
//             throw new Error("Age should be in range")
//         }
//         this._age = val
//     }

//     public getDetails() {
//         return `Hi, I am ${this.lastName} ${this.firstName}. I am ${this.age} years old!`
//     }

// }

// let maria = new Person("Maria", "Doe", true);
// console.log(maria.getDetails())
// console.log("Is Maria Graduated : ", maria.isGraduated)

// maria.age = 17
// console.log("Maria Age : ", maria.age)

// maria.isGraduated = false;           // Will thorw the error



// class Person {
//     private firstName: string;
//     private lastName: string;

//     constructor(firstName: string, lastName: string) {
//         this.firstName = firstName;
//         this.lastName = lastName
//     }
//     getFullName() { return `${this.lastName} ${this.firstName}` }
//     protected getDetails() {
//         return `Hi, I am ${this.lastName} ${this.firstName}.`
//     }
// }

// class Student extends Person {
//     private static numberOfStudent: number = 0;
//     constructor(firstName: string, lastName: string, private studId: string) {
//         super(firstName, lastName)
//         Student.numberOfStudent++
//     }
//     getDetails(): string {
//         return super.getDetails() + ` My Student ID - ${this.studId}`
//     }
//     static getNumberOfStudents() {
//         return Student.numberOfStudent;
//     }
// }

// let john = new Student("John", "Doe", "S001")
// let jenny = new Student("jenny", "Doe", "S002")
// console.log(john.getFullName())
// console.log(john.getDetails())
// // console.log("Number of Students : ", Student.numberOfStudent)
// console.log("Number of Students : ", Student.getNumberOfStudents())




// Abstract Class - one or more abstract methods
// abstract class Student {

//     constructor(private firstName: string, private lastName: string) { }

//     get getFullName() {
//         return `${this.firstName} ${this.lastName}`;
//     }

//     abstract getFee(): number;

//     feeStatement() {
//         return `${this.getFullName} pays ${this.getFee()} to the institution.`;
//     }
// }

// class NonRegularStudent extends Student {
//     constructor(
//         firstName: string,
//         lastName: string,
//         private numOfCourse: number,
//         private feePerCourse: number) {
//         super(firstName, lastName)
//     }
//     getFee(): number {
//         return this.feePerCourse * this.numOfCourse;
//     }
// }

// let jenny = new NonRegularStudent("Jenny", "Doe", 2, 2000);
// console.log(jenny.feeStatement())


// class RegularStudent extends Student {
//     constructor(firstName, lastName, private fee: number) {
//         super(firstName, lastName)
//     }
//     getFee(): number {
//         return this.fee;
//     }
// }

// let alice = new RegularStudent("Alice", "Doe", 6000);
// console.log(alice.feeStatement())




// Interface

interface Person {
    firstName: string;
    lastName: string;
    age: 32;
}
interface ConvertInJSON {
    toJSON(): string
}
class Student implements ConvertInJSON {
    private firstName: string;
    private lastName: string;
    private age: number;

    constructor(person: Person) {
        this.firstName = person.firstName;
        this.lastName = person.lastName;
        this.age = person.age;
    }
    toJSON(): string {
        console.log(this);
        return JSON.stringify({ fullName: this.firstName + this.lastName, age: this.age })
        // return JSON.stringify(this)
    }
}

let obj: Person = {
    firstName: "Foo",
    lastName: "Bar",
    age: 32
}

let foo = new Student(obj)
console.log(foo.toJSON())
