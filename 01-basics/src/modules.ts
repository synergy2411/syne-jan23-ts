// Named Import
import { getLuckyNumber } from './utils/fortune'
import * as fortune from './utils/fortune';

// Default Import
import add from './utils/maths';

console.log("Your lucky number today : ", getLuckyNumber())

console.log("Sum is : ", add(20, 13))

console.log("Today's quote : ", fortune.getDailyQuote())