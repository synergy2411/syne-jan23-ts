

// Intersection Operator (&)

// interface IStudent {
//     studId : string;
//     studName : string;
// }

// interface IEmployee {
//     empId : string;
//     empName : string;
// }

// type EitherStudentOrEmployee = IStudent | IEmployee;                // Union Operator

// let studOrEmp : EitherStudentOrEmployee = {
//     studId : "",
//     studName :""
// }

// let empOrStud : EitherStudentOrEmployee = {
//     empId :"e001",
//     empName : "Foo Bar"
// }


// interface IPerson {
//     firstName: string;
//     lastName: string;
//     address : string;
// }

// interface IStudent {
//     studId: string;
//     courseName: string;
//     address : string;
// }

// type PersonStudentType = IPerson & IStudent;                //Intersection Operator

// let foobar: PersonStudentType = {
//     firstName: "Foo",
//     lastName: "Bar",
//     studId: "s001",
//     courseName: "TypeScript",
//     address : "201 Main road, Chinchwad"
// }

// type MyType = string | number;

// let abcd : MyType = ""
// let xyza : MyType = 32

