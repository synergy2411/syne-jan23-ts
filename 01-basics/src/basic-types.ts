// Implicit type / Type inference
// Use implicit types as much as you can.

// let age = 32;

// age = true;

// age = "Hello"

// age = {}

// age = 101;

// console.log(age)

// let username = "John Doe";

// username = 32;

// console.log(username)


// Explicit Type / Type Annotation
// - Whenever we don't want to supply the value while creating the variable

// let xyz: boolean;

// xyz = true;

// xyz = 32;

// console.log("XYZ : ", xyz)

// let abc = "Hello World";



// Types in TypeScript

let xyz: unknown = "Hello World";
let abc: any = "Welcome to TypeScript";
let username = "John Doe";

username = abc;

console.log("Username : ", username);

if (typeof xyz === 'string') {
    username = xyz;
}


abc = xyz;


// function getProducts(productId : string) :unknown {
//     // makes XHR Call
//     return {
//         id : productId,
//         prodName :"iPhone 14"
//     }
// }

// const product = getProducts("p001")

// let newProduct : { id : string, prodName : string };

// newProduct = <{id : string, prodName : string}> product;


// function getUsername(id: string): unknown {
//     return "John Doe"
// }

// const userTwo: string = <string>getUsername("u001")


// if (typeof xyz === 'string') {
//     xyz = username;
// }

// xyz = "Hello";
// console.log("XYZ : ", xyz);

// if (typeof xyz === 'number') {
//     xyz = 32;
// }

// console.log("XYZ : ", xyz);



// abc = "Welcome";

// abc = 101;

// xyz = abc;

// abc = xyz;

let userAge: number = 32;
let userName: string = "John Doe";
let isUserAdmin: boolean = true;

let userFriends: string[] = ["Joe", "Monica", "Ross"];
// userFriends[0].

let numbers: Array<number> = [101, 102, 103]
// numbers[0].

type Address = { street: string, city: string, pinCode: number, country?: string };

let userAddress: Address = {
    street: "201 Main Road",
    city: "Pune",
    pinCode: 234567
}

// userAddress.

type MyFun = (name: string) => string;

let demoFn: MyFun = (name: string) => {
    return `Hello from ${name}`
}

let message = demoFn("John Doe");

// Type Alias - 'type' keyword
type MyFunTwo = () => void;

let userDetails: MyFunTwo = function () {
    console.log("Hello There!");
}

let userDob: Date = new Date("Dec 12, 1984")

enum Color {
    Red = 101,
    Green,
    Blue
}

let favColor: Color = Color.Green;

console.log("Fav Color : ", favColor)



// Tuple Type
// - Restricts number of element in Array
// - Typed Elements only 

// let arr : Array<number>= []

let arr: [string, number] = ["John Doe", 32]

// let arrTwo : [number, string] = [101, "Ross"];
// let arrTwo : [number, string] = ["Ross", 101];
// let arrTwo : [number, string] = [101, "Ross", "Monica"];


function throwError(): never {
    throw new Error("Something wrong happened here")
}


// Union Type
let theAge: string | number;

theAge = "Thiry-two";

theAge = 32;

// String Literal Type
let theName: "John";

theName = "John"

// Null Type

let postData: Array<string> | null = null;

postData = ["", "", ""];






