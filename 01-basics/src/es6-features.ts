

// Rest / Spread (...)
// - Rest Operator : creates the collection from the individual elements
// - Usage : in function arguments only; must be the last argument in function argument list

// const dummyFn = (email: string, ...args) => {
//     console.log(args[0]);               // 32
// }

// dummyFn("test@test")
// dummyFn("test@test", 32)
// dummyFn("test@test", 32, true)


// - Spread : spreads the collection into individual elements

// let userOne = {
//     email: "test@test",
//     age: 32
// }

// let userTwo = {
//     ...userOne,                     // { email, age }
//     address: "201 Main road"
// }

// console.log(userTwo);       // { email, age, address }

// let arrOne = [3, 4, 5];

// let arrTwo = [1, 2, ...arrOne, 6, 7, 8];

// console.log(arrTwo);            // [ 1,2,3,4,5,6,7,8 ]

// const dummyTwo = (...args) => {}

// let userThree = { email : "foo@test", age : 32, isAdmin : true}
// let userFour = ["bar@test", 32, false]

// dummyTwo(...userFour)



// Destructuring : "unpacking the collection"
// - Array : indexed collection; ordered collection
// - Object : unordered collection

let friends = ["foo", "bar", "bam", "bas"];

let [f1, f2, f3, f4] = friends;

console.log(f3)


let user = {
    firstName: "John",
    lastName: "Doe",
    address: {
        city: "Pune",
        street: "201, Main Road, Wakad"
    }
}


let { lastName, firstName, address: { city, street } } = user;

console.log(lastName, firstName, street, city);



let users = [
    { email: "foo@test", age: 32 },
    { email: "bar@test", age: 33 },
    { email: "bam@test", age: 34 },
    { email: "bas@test", age: 35 },
]

let [{ email: e1, age: a1 }, { email: e2, age: a2 }, { email: e3, age: a3 }, { email: e4, age: a4 }] = users;

console.log(e1, a1, e3, a3);


let me = {
    friends: ["Joe", "Chandler", "Ross"]
}

let { friends: [joe, chandler, ross] } = me;



let [fr1, fr2, fr3] = [...me.friends]





let moreFriends = ["Rachel", "ALice"];

let [a, b] = moreFriends;

[a, b] = [b, a]

console.log(a, b)           // a: 