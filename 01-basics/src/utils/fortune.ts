const MAGIC_NUMBER = Math.round(Math.random() * 100)

const getLuckyNumber = () => MAGIC_NUMBER;

const getDailyQuote = () => "Run 5 miles today!"

export { getLuckyNumber, getDailyQuote }