// class Person {
//     // private firstName: string;
//     // private lastName: string;
//     // private age: number;
class Student {
    constructor(person) {
        this.firstName = person.firstName;
        this.lastName = person.lastName;
        this.age = person.age;
    }
    toJSON() {
        console.log(this);
        return JSON.stringify({ fullName: this.firstName + this.lastName, age: this.age });
        // return JSON.stringify(this)
    }
}
let obj = {
    firstName: "Foo",
    lastName: "Bar",
    age: 32
};
let foo = new Student(obj);
console.log(foo.toJSON());
//# sourceMappingURL=demo.js.map