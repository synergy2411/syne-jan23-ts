"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getDailyQuote = exports.getLuckyNumber = void 0;
const MAGIC_NUMBER = Math.round(Math.random() * 100);
const getLuckyNumber = () => MAGIC_NUMBER;
exports.getLuckyNumber = getLuckyNumber;
const getDailyQuote = () => "Run 5 miles today!";
exports.getDailyQuote = getDailyQuote;
//# sourceMappingURL=fortune.js.map