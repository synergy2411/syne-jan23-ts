// GENERICS : placeholder to hold the 'type' value
// GENERIC CONSTRAINTS
// function demo<T,K extends T>(val : T, arr : K) : K | T {
//     return arr;
// }
// type IUser = { name : string }
// type IStudent = { name : string, studId : string}
// let tUser : IUser = { name: "John" }
// let kStudent : IStudent = { name : "James", studId : "S001"} 
// const newUser = demo<IUser, IStudent>(tUser, kStudent)
function getLength(args) {
    return args['length'];
}
const arrLength = getLength(["John", "Jenny", "James"]);
console.log("Array Length : ", arrLength);
// const numLength = getLength<number>(3)
// console.log("Number Length : ", numLength);
const strLength = getLength("Welcome to typescript");
console.log("String Length : ", strLength);
function merge(obj1, obj2) {
    return Object.assign(Object.assign({}, obj1), obj2);
}
const newObj2 = merge({ name: "Jenny" }, { email: "jenny@test" });
console.log(newObj2);
const newObj = merge({ name: "John" }, { email: "test@test" });
console.log(newObj);
// GENERIC INTERFACE
// interface Resource<T, U>{
//     resourceId :T;
//     resourceName : U;
// }
// let server : Resource<string, string> = {
//     resourceId : "S001",
//     resourceName : "Server 01"
// }
// let serverTwo : Resource<number, string> = {
//     resourceId : 1011,
//     resourceName : "Server 02"
// }
// interface ICollection<T> {
//     add(val: T): void;
//     remove(val: T): void
// }
// class List<T> implements ICollection<T>{
//     private items: T[] = [];
//     add(val: T): void {
//         this.items.push(val);
//     }
//     remove(val: T): void {
//         const position = this.items.indexOf(val)
//         if (position > -1) {
//             this.items.splice(position, 1);
//             return;
//         }
//         throw new Error("Element not found - " + val)
//     }
//     getItems(): T[] {
//         return this.items;
//     }
// }
// let myList = new List<number>();
// myList.add(10)
// myList.add(15)
// myList.add(20)
// console.log("AFTER ADD : Number List : ", myList.getItems());
// myList.remove(15)
// console.log("AFTER REMOVE : Number List : ", myList.getItems());
// GENERIC CLASS
// class Stack<T>{
//     private items: T[] = [];
//     constructor(private size: number) { }
//     push(item: T) {
//         this.items.push(item)
//     }
//     pop(): T {
//         return this.items.pop()
//     }
//     getItems(): T[] {
//         return this.items;
//     }
//     isListFull(): boolean {
//         return this.items.length === this.size;
//     }
// }
// let strList = new Stack<string>(3)
// strList.push("Welcome")
// strList.push("To")
// // strList.push("TypeScript")
// console.log("String List : ", strList.getItems());
// console.log("Stack is full ? ", strList.isListFull());
// let numList = new Stack<number>(10);
// numList.push(101);
// console.log("AFTER PUSH : ", numList.getItems())
// console.log("Popped Number : ", numList.pop())
// console.log("AFTER POP : ", numList.getItems())
// GENERIC FUNCTIONS
// function add<T>(n1 : T, n2: T) : T{          // NOT A GOOD CASE TO USE GENERICS
//     if(typeof n1 === 'number' && typeof n2 === 'number'){
//         return n1 + n2
//     }
// }
// add<number>(2,4)
// function addAtBegining<T>(val: T, arr: T[]): T[] {
//     return [val, ...arr]
// }
// type IUser = { name: string, age: number }
// let moreUser: Array<IUser> = [
//     { name: "John", age: 32 }, { name: "Jenny", age: 34 }
// ]
// const myUser = addAtBegining<IUser>({ name: "Foo", age: 30 }, moreUser)
// myUser[0].
// const numArrTwo = addAtBegining<number>(101, [102, 103, 104])
// const strArrTwo = addAtBegining<string>("Hello", ["I", "am", "Alice"])
// const numArray = addAtBegining(101, [102, 103, 104])
// console.log(numArray);
// const strArray = addAtBegining("Hello", ["I", "am", "Alice"])
// console.log(strArray);
// function addStringAtBeginning (val : string, arr : string[]) : string[] {
//     return [val, ...arr]
// }
// const strArray = addStringAtBeginning("Hello", ["I","am", "John", "Doe"])
// function addNumberAtBeginning (val : number, arr : number[]) : number[] {
//     return [val, ...arr]
// }
// const numArray = addNumberAtBeginning(101, [102, 103, 104, 105])
//# sourceMappingURL=generics.js.map