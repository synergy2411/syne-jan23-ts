// Implicit type / Type inference
// Use implicit types as much as you can.
// let age = 32;
// age = true;
// age = "Hello"
// age = {}
// age = 101;
// console.log(age)
// let username = "John Doe";
// username = 32;
// console.log(username)
// Explicit Type / Type Annotation
// - Whenever we don't want to supply the value while creating the variable
// let xyz: boolean;
// xyz = true;
// xyz = 32;
// console.log("XYZ : ", xyz)
// let abc = "Hello World";
// Types in TypeScript
let xyz = "Hello World";
let abc = "Welcome to TypeScript";
let username = "John Doe";
username = abc;
console.log("Username : ", username);
if (typeof xyz === 'string') {
    username = xyz;
}
abc = xyz;
// function getProducts(productId : string) :unknown {
//     // makes XHR Call
//     return {
//         id : productId,
//         prodName :"iPhone 14"
//     }
// }
// const product = getProducts("p001")
// let newProduct : { id : string, prodName : string };
// newProduct = <{id : string, prodName : string}> product;
// function getUsername(id: string): unknown {
//     return "John Doe"
// }
// const userTwo: string = <string>getUsername("u001")
// if (typeof xyz === 'string') {
//     xyz = username;
// }
// xyz = "Hello";
// console.log("XYZ : ", xyz);
// if (typeof xyz === 'number') {
//     xyz = 32;
// }
// console.log("XYZ : ", xyz);
// abc = "Welcome";
// abc = 101;
// xyz = abc;
// abc = xyz;
let userAge = 32;
let userName = "John Doe";
let isUserAdmin = true;
let userFriends = ["Joe", "Monica", "Ross"];
// userFriends[0].
let numbers = [101, 102, 103];
let userAddress = {
    street: "201 Main Road",
    city: "Pune",
    pinCode: 234567
};
let demoFn = (name) => {
    return `Hello from ${name}`;
};
let message = demoFn("John Doe");
let userDetails = function () {
    console.log("Hello There!");
};
let userDob = new Date("Dec 12, 1984");
var Color;
(function (Color) {
    Color[Color["Red"] = 101] = "Red";
    Color[Color["Green"] = 102] = "Green";
    Color[Color["Blue"] = 103] = "Blue";
})(Color || (Color = {}));
let favColor = Color.Green;
console.log("Fav Color : ", favColor);
// Tuple Type
// - Restricts number of element in Array
// - Typed Elements only 
// let arr : Array<number>= []
let arr = ["John Doe", 32];
// let arrTwo : [number, string] = [101, "Ross"];
// let arrTwo : [number, string] = ["Ross", 101];
// let arrTwo : [number, string] = [101, "Ross", "Monica"];
function throwError() {
    throw new Error("Something wrong happened here");
}
// Union Type
let theAge;
theAge = "Thiry-two";
theAge = 32;
// String Literal Type
let theName;
theName = "John";
// Null Type
let postData = null;
postData = ["", "", ""];
//# sourceMappingURL=basic-types.js.map