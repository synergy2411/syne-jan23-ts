// @ClassDecorator
// class Test{
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
//     @PropertyDecorator
//     _username : string;
//     @MethodDecorator
//     getUser( args1 : string, @PropertyDecorator args2 : string ){}
//     @AccessorDecorator
//     get username(){
//         return this._username;
//     }
// }
// Decorator Factory
// function Seal(val) {
//     return (target) => {
//         Object.seal(target);
//         Object.seal(target.prototype);
//     }
// }
// @Seal({})
// class Test {}
let myFriends = ["Joe", "Monica", "Ross"];
function FriendOnly(oldFriend) {
    return (target, propertyName) => {
        let currentValue = target[propertyName] || oldFriend;
        Object.defineProperty(target, propertyName, {
            get: () => currentValue,
            set: (newValue) => {
                if (myFriends.includes(newValue)) {
                    currentValue = newValue;
                }
                return;
            }
        });
    };
}
function PrintArgs(target, propertyName, paramIndex) {
    console.log("Index : ", paramIndex);
}
class MyFriends {
    getFriend(args, args2) {
        console.log("ARGS : ", args);
        console.log("ARGS 02: ", args2);
    }
}
__decorate([
    FriendOnly("Joe"),
    __metadata("design:type", String)
], MyFriends.prototype, "friendName", void 0);
__decorate([
    __param(1, PrintArgs),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, String]),
    __metadata("design:returntype", void 0)
], MyFriends.prototype, "getFriend", null);
let theFriend = new MyFriends();
theFriend.friendName = "Xyz";
console.log("After XYZ : ", theFriend.friendName);
theFriend.friendName = "Monica";
console.log("After Monica : ", theFriend.friendName);
theFriend.getFriend("Rachel", "Chandler");
//# sourceMappingURL=decorators.js.map