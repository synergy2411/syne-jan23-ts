// Function Overload : defines the relationship between the supplied arguments and return type of the function
// Different number of Parameter
// function add(num1: number, num2: number): number;
// function add(num1: number, num2: number, num3: number): number;
// function add(num1: number, num2: number, num3?: number): number {
//     if (num3) {
//         return num1 + num2 + num3
//     }
//     return num1 + num2;
// }
// console.log("Two Number : ", add(2, 4))
// console.log("Three Number : ", add(2, 4, 6));
// Different Types of Parameter
// function add(num1 : string, num2 : string) : string;
// function add(num1 : number, num2:number) : number;
// function add(num1 : any, num2 :any): any{
//     return num1 + num2;
// }
// const result = add(2,4)
// const strResult = add("Hello", "World")
// function add(val1: number | string, val2: number | string): number | string {
//     if (typeof val1 === 'number' && typeof val2 === 'number') {
//         return val1 + val2
//     }
//     if (typeof val1 === 'string' && typeof val2 === 'string') {
//         return val1.concat(val2)
//     }
// }
// const numResult = add(2, 2)
// // // numResult.
// const strResult = add("Hello", "World")
// strResult.
// Intersection Operator (&)
// interface IStudent {
//     studId : string;
//     studName : string;
// }
// interface IEmployee {
//     empId : string;
//     empName : string;
// }
// type EitherStudentOrEmployee = IStudent | IEmployee;                // Union Operator
// let studOrEmp : EitherStudentOrEmployee = {
//     studId : "",
//     studName :""
// }
// let empOrStud : EitherStudentOrEmployee = {
//     empId :"e001",
//     empName : "Foo Bar"
// }
// interface IPerson {
//     firstName: string;
//     lastName: string;
//     address : string;
// }
// interface IStudent {
//     studId: string;
//     courseName: string;
//     address : string;
// }
// type PersonStudentType = IPerson & IStudent;                //Intersection Operator
// let foobar: PersonStudentType = {
//     firstName: "Foo",
//     lastName: "Bar",
//     studId: "s001",
//     courseName: "TypeScript",
//     address : "201 Main road, Chinchwad"
// }
// type MyType = string | number;
// let abcd : MyType = ""
// let xyza : MyType = 32
//# sourceMappingURL=day02.js.map