// let x  = 101;

// console.log(typeof x);          // number

// x = "Hello"

// console.log(typeof x);          // string

// x = { name: "Foo" }

// console.log(typeof x);

// x = new Date("Jan 8, 2023")

// console.log(typeof x);

// x = true;

// console.log(typeof x);

// x = ["Hello", "World"]
// console.log(typeof x);

// x = function () { }
// console.log(typeof x);

// x = null;
// console.log(typeof x);


function getUserDetails(userId) {
    return {
        id: userId,
        userName: "John Doe",
        email: "test@test.com",
        age: 32
    }
}

const user = getUserDetails("u001");

function displayUserDetails(username, age) {
    console.log(`The user name ${username} is having email of ${age}`)
}

displayUserDetails(user.age, user.userName)


